export { setupQuasar, setupI18n, echarts } from '@herodotus/plugins';

export * from './pinia';
export * from './highlight';
